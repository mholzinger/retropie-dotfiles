alias apt-get='sudo apt-get'
alias apt-search='sudo apt-cache search'
alias retropie_setup='sudo /home/pi/RetroPie-Setup/retropie_setup.sh'

# Transfer.sh uploading
transfer() { if [ $# -eq 0 ]; then echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile;echo; rm -f $tmpfile; }

upload_rom(){
rom="$1";
for i in sha1 md5
  do openssl $i $rom
done
ls -lahtr $rom | awk '{print "Size - "$5" ["$NF"]"}'
transfer $rom
}

# A retropie emulator core has been launched and has taken over control of the pie. Stop that emulator core
stop_core(){
  ps aux |grep -iE '[R]uncommand.sh|[R]etroarch'|awk '{print $2}'| xargs -L 1 kill
}

# This command needs to actually be replaced by a check in cron, or a fancier mount command in init
# We are looking to see if the usb assignment has changed and call to remount to the expected mapping
remount_usb(){
  sudo umount /media/usb*
  sudo mount -t vfat /dev/sda1 /media/usb
}

