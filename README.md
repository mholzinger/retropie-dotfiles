# retropie-dotfiles

`.bash_aliases` commands for RetroPie

To add this to your bash aliases, run 

```
cat bash_aliases >> ~/.bash_aliases
. ~/.bash_aliases
```

#### Stubs for apt commands:

---

$ **apt-get** - is an alias to 'sudo apt-get'

$ **apt-search** - is an alias to 'sudo apt-cache search'

$ **retropie_setup** - calls to launch the retropie_setup.sh script

---

#### Transfer.sh 

https://transfer.sh commands have been added to allow a way to hop into the raspberrypi from a terminal and share a link to an uploaded save file, config file or modified rom

$ **transfer \</path/to/filename\>**

$ **upload_rom \</path/to/rom\>**

---

$ **stop_core** - Stop any running retropie core dead in it's tracks

---

$ **remount_usb** - This command assumes that the flashdrive mounted on `/media/usb` has been unintentionally remapped and calls to remount to the expected mapping.

_example:_

```
sudo umount /media/usb*
sudo mount -t vfat /dev/sda1 /media/usb
```

Todo: This command should  actually be replaced by a check in cron, or a fancier mount command in init
